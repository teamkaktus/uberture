<?php echo $header; ?><?php if( ! empty( $mfilter_json ) ) { echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>'; } ?>
<div class="content">
    <div class="container">
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php if($breadcrumb == end($breadcrumbs)) { ?>
            <li><?php echo $breadcrumb['text']; ?></li>
            <?php } else { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
            <?php } ?>
        </ul>
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="main <?php echo $class; ?>"><?php echo $column_right; ?>
                <div class="main__title"><?php echo $heading_title; ?></div><?php echo $content_top; ?><div id="mfilter-content-container">
                <?php if ($categories) { ?>
                <?php } ?>
                <?php if ($products) { ?>
                <div class="catalog">
                    <?php foreach ($products as $product) { ?>
                    <div class="tovar-item">
                        <div class="tovar-item__pict">
                            <a href="<?php echo $product['href']; ?>">
                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                     title="<?php echo $product['name']; ?>"/></a>
                        </div>
                        <div class="tovar-item__title">
                            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                        </div>
                        <div class="tovar-item__price">
                            <?php if ($product['price']) { ?>
                            <?php if (!$product['special']) { ?>
                            <?php echo $product['price']; ?> <span>е</span>
                            <?php } else { ?>
                            <?php echo $product['price']; ?> <span>е</span>
                            <?php } ?>
                            </p>
                            <?php } ?>
                        </div>
                        <a href="<?php echo $product['href']; ?>" class="tovar-item__link">Подробнее</a>
                    </div>
                    <?php } ?>
                </div>
                <!--<div class="row">
                    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                </div>-->
                <?php } ?>
                <?php if (!$categories && !$products) { ?>
                <p><?php echo $text_empty; ?></p>
                <div class="buttons">
                    <div class="pull-right"><a href="<?php echo $continue; ?>"
                                               class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                </div>
                <?php } ?>
                </div><?php echo $content_bottom; ?></div>
            </div>
    </div>
</div>
<?php echo $footer; ?>