<footer class="footer">
  <div class="container">
    <div class="footer-block">
      <a href="#" class="footer-block__logo"><img src="img/logo-footer.png" alt=""></a>
      <div class="footer-block__location">
        <?php echo $findTouch; ?>
      </div>
      <a href="#" class="footer-block__link">Схема проезда</a>
      <div class="footer-block__phone">
        <a href="tel:+73432478248" class="footer-block__tel">
          <?php echo $findTelef; ?>
        </a>,
        <a href="tel:+73432478248" class="footer-block__tel">
          <?php echo $findTelef; ?>
        </a>
      </div>
      <a href="mailto:email@email.ru" class="footer-block__mail">
        <?php echo $findEmail; ?>
      </a>
      <div class="social-block">
        <span><?= $Subscribe_to_us ?>:</span>
        <a href="#" class="social-block__link social-block__link--inst">instagram</a>
        <a href="#" class="social-block__link social-block__link--fb">facebook</a>
        <a href="#" class="social-block__link social-block__link--vk">vkontakte</a>
      </div>
      <a href="#" class="link__letter"><?= $Write_a_letter ?></a>
    </div>

    <div class="footer-menu footer-menu--about">
      <?php if ($informations) { ?>
      <div class="footer-menu__title">
        <?php echo $text_information; ?>
      </div>
      <ul class="footer-nav">
        <?php foreach ($informations as $information) { ?>
        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <?php } ?>
    <div class="footer-menu footer-menu--catalog">
      <div class="footer-menu__title footer-menu__title--low"><?= $catalog?></div>
      <ul class="footer-nav">
        <?php foreach ($categories as $category) { ?>
        <li><a href="<?php echo $category['name']; ?>"> <?php echo $category['name']; ?></a></li>
        <?php } ?>

      </ul>
    </div>

    <div class="footer-menu footer-menu--order">
      <div class="footer-menu__title footer-menu__title--up"><?= $How_to_order; ?></div>
      <ul class="footer-nav">
        <li><a href="#">Порядок заказа</a></li>
        <li><a href="#">Наши гарантии</a></li>
        <li><a href="#">Доставка и монтаж</a></li>
        <li><a href="#">Вопрос-ответ</a></li>
        <li><a href="#">Вопрос-ответ</a></li>
      </ul>
    </div>

    <div class="footer-menu">
      <div class="footer-menu__title footer-menu__title--up"><?= $Our_works; ?></div>
      <ul class="footer-nav">
        <li><a href="#">Установленные двери</a></li>
        <li><a href="#">Статьи</a></li>
      </ul>
    </div>

  </div>
</footer>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/common2.js" type="text/javascript"></script>

<script src="catalog/view/javascript/libs/modernizr/modernizr.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/formstyler/jquery.formstyler.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/touchSwipe/jquery.touchSwipe.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/slick/slick.min.js" type="text/javascript"></script>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->


            
</body></html>