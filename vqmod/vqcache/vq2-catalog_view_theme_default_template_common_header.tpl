<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
  <!--<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />-->
				
				<script src="catalog/view/javascript/mf/jquery-ui.min.js" type="text/javascript"></script>
			
  <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
  <!--<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">-->
<link href="catalog/view/theme/default/stylesheet/sprite.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/fonts.css" rel="stylesheet">
  <link href="catalog/view/javascript/libs/normalize/normilize.css"  rel="stylesheet"  type="text/css">
  <link href="catalog/view/javascript/libs/magnific/magnific-popup.css"  rel="stylesheet"  type="text/css">
  <link href="catalog/view/javascript/libs/formstyler/jquery.formstyler.css"  rel="stylesheet"  type="text/css">
  <link href="catalog/view/javascript/libs/slick/slick.css"  rel="stylesheet"  type="text/css">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

            
</head>
<body class="<?php echo $class; ?>">
<header class="header">
  <div class="header-top">
    <div class="container">
      <a href="#" class="sale-link">Акции</a>
      <ul class="menu-top">
        <li class="menu-top__item"><a href="index.php?route=information/information&information_id=4">О нас</a></li>
        <li class="menu-top__item"><a href="index.php?route=information/news">Новости</a></li>
        <li class="menu-top__item"><a href="index.php?route=information/gallery">Каталог</a></li>
        <li class="menu-top__item"><a href="index.php?route=information/information&information_id=7">Как заказать</a></li>
        <li class="menu-top__item"><a href="index.php?route=information/information&information_id=8">Вопрос-ответ</a></li>
        <li class="menu-top__item"><a href="index.php?route=information/contact">Контакты</a></li>
      </ul>
    </div>
  </div>
  <div class="header-main">

    <div class="container">
      <span class="logo">
        <?php if ($logo) { ?>
        <?php if ($home == $og_url) { ?>
        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/>
        <?php } else { ?>
        <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/></a>
        <?php } ?>
        <?php } else { ?>
        <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
        <?php } ?>
      </span>
      <div class="cart"><?php echo $cart; ?></div>
      <div class="work">
        <p><?php echo $open; ?></p>
      </div>
      <div class="location location--header">
        <p><span><?php echo $address; ?></span><?php echo $address2; ?></p>
        <a href="#" class="location__link">Схема проезда</a>
      </div>
      <div class="contact contact--header">
        <div class="contact-wrap">
          <a href="+73432478248" class="contact__phone contact__phone--header"><?php echo $telephone; ?></a>
          <a href="+73432478248" class="contact__phone contact__phone--header"><?php echo $telephone2; ?></a>
          <a href="#" class="contact__callback">Перезвоните мне!</a>
        </div>
        <a href="mailto:email@mail.com" class="contact__mail"><?php echo $email; ?></a>
      </div>
    </div>

  </div>
  <div class="header-bottom">
    <div class="container">
      <ul class="menu-main">
        <li class="menu-main__item"><a href="#">Входные двери</a></li>
        <li class="menu-main__item"><a href="#">Межкомнатные двери</a></li>
        <li class="menu-main__item"><a href="#">Фурнитура</a></li>
        <li class="menu-main__item"><a href="#">Ламинат</a></li>
        <li class="menu-main__item">
          <a href="#">Наши услуги<i class="caret"></i></a>
          <ul class="menu-dropmain">
            <li><a href="#">Межкомнатные двери</a></li>
            <li><a href="#">Входные двери</a></li>
            <li><a href="#">Ламинат</a></li>
          </ul>
        </li>
        <li class="menu-main__item"><a href="index.php?route=information/gallery">Галерея</a></li>
      </ul>
    </div>
  </div>
  <div class="container">
</header>
<!--/*<?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?>*/-->
