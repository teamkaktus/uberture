$(document).ready(function () {
    $('#send').on('click', function (even) {
        even.preventDefault();
        if (($('#phone').val().length < 3) && ($('#name').val().length < 3)){
            $('.phone').show();
            $('.name').show();
            } else if ($('#name').val().length < 3) {
             $('.name').show();
             $('.phone').hide();
        } else if ($('#phone').val().length < 3) {
            $('.phone').show();
            $('.name').hide()
        } else {
            $('.name').hide()
            $('.phone').hide();
            var res = $('#contactForm_c').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            swal("Сообщение отправлено", "", "success");
            $("button.close").trigger("click");
            $('#contactForm').removeClass('in');
            $('.modal-backdrop').removeClass('in');
            $.ajax({
                url: 'index.php?route=common/header/contactForm',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function () {
                }
            });
            $('#name').val('');
            $('#phone').val('');
        }
    });
});

$(document).ready(function () {
      $('#email_send').on('click', function (even) {
        even.preventDefault();
        if (($('#email_name').val().length < 3) && !($('#email_email').val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i)) && ($('#email_massage').val().length < 10)) {
            $('#err_e_name').show();
            $('#err_e_email').show()
            $('#err_e_massage').show()
        }else if(($('#email_name').val().length < 3) && !($('#email_email').val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i))) {
            $('#err_e_name').show();
            $('#err_e_email').show()
            $('#err_e_massage').hide()
        }else if(($('#email_name').val().length < 3) && ($('#email_massage').val().length < 10)) {
            $('#err_e_name').show();
            $('#err_e_email').hide()
            $('#err_e_massage').show()
        }else if(!($('#email_email').val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i)) && ($('#email_massage').val().length < 10)){
            $('#err_e_name').hide();
            $('#err_e_email').show()
            $('#err_e_massage').show()
        }else if($('#email_name').val().length < 3){
            $('#err_e_name').show();
            $('#err_e_email').hide();
            $('#err_e_massage').hide();
        }else if(!($('#email_email').val().match(/^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i))){
            $('#err_e_name').hide();
            $('#err_e_email').show();
            $('#err_e_massage').hide();
        }else if($('#email_massage').val().length < 10) {
            $('#err_e_name').hide();
            $('#err_e_email').hide();
            $('#err_e_massage').show();
        }else{
            var res = $('#modal_email_1').serializeArray();
            var arr = {};
            $.each(res, function (result) {
                var $index = res[result].name;
                arr[$index] = res[result].value;
            });
            $.ajax({
                url: 'index.php?route=common/footer/contactFormEmail',
                type: 'post',
                dataType: 'json',
                data: arr,
                success: function (data) {
                }
            });
            $('#err_e_name').hide();
            $('#err_e_email').hide();
            $('#err_e_massage').hide();
            $('#email_name').val('');
            $('#email_email').val('');
            $('#email_massage').val('');
            swal("Сообщение отправлено", "", "success");
            $("button.close").trigger("click");
        }
    });
});