<!-- 
	Ajax Quick Checkout 
	v6.0.0
	Dreamvention.com 
	d_quickcheckout/confirm.tpl 
-->

<script type="text/html" id="confirm_template">
<div id="confirm_wrap">
	<div class="form_order1">
		<div class="panel-body">
			<form id="confirm_form" class="form-horizontal">
			</form>
			
			<button id="qc_confirm_order" class="btn btn--order" <%= model.show_confirm ? '' : 'disabled="disabled"' %>><?php echo $button_confirm; ?></button>

		</div>
	</div>
</div>
</script>
<script>

$(function() {
	qc.confirm = $.extend(true, {}, new qc.Confirm(<?php echo $json; ?>));
	qc.confirmView = $.extend(true, {}, new qc.ConfirmView({
		el:$("#confirm_view"), 
		model: qc.confirm, 
		template: _.template($("#confirm_template").html())
	}));
});

</script>