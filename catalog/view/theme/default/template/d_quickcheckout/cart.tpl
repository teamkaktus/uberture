<!-- 
	Ajax Quick Checkout 
	v6.0.0
	Dreamvention.com 
	d_quickcheckout/cart.tpl 
-->
<script type="text/html" id="cart_template">

	<div class="panel panel-default <%= model.config.display ? '' : 'hidden' %>">
		<div class="main__title">Корзина / Оформление заказа</div>

		<div class="block-table" >
			<% if(model.error){ %>
				<div class="alert alert-danger">
					<i class="fa fa-exclamation-circle"></i> <%= model.error %>
				</div>
			<% } %>
					<div class="block-table__row block-table__row--head">
						<div class="block-table__cell block-table__cell--head <%= parseInt(model.config.columns.image) ? '' : 'hidden' %>"><?php echo $column_image; ?></div>
						<div class="block-table__cell block-table__cell--head <%= parseInt(model.config.columns.name) ? '' : 'hidden' %>"><?php echo $column_name; ?></div>
						<div class="block-table__cell block-table__cell--head <%= parseInt(model.config.columns.quantity) ? '' : 'hidden' %>"><?php echo $column_quantity; ?></div>
						<div class="block-table__cell block-table__cell--head <%= parseInt(model.config.columns.price) && model.show_price ? '' : 'hidden' %>"><?php echo $column_price; ?></div>
						<div class="block-table__cell block-table__cell--head <%= parseInt(model.config.columns.total) && model.show_price ? '' : 'hidden' %>"><?php echo $column_total; ?></div>
					</div>

					<% _.each(model.products, function(product) { %>
			<div class="block-table__row block-table__row--body">
						<div class="block-table__cell block-table__cell--body block-table__cell--pict <%= parseInt(model.config.columns.image) ? '' : 'hidden' %>">
							<div class="cart-pict">
								<img src="<%= product.thumb %>" alt=""/>
							</div>
						</div>

						<div class="block-table__cell block-table__cell--body block-table__cell--title <%= parseInt(model.config.columns.name) ? '' : 'hidden' %>">
							<a href="<%= product.href %>" class="cart-title" <%=  model.config.columns.image ? '' : 'rel="popup" data-help=\'<img src="' + product.image + '"/>\'' %>>
								<%= product.name %> <%= product.stock ? '' : '<span class="out-of-stock">***</span>' %>
							</a>
							<ul class="cart-list">
								<% $i = 0; %>
								<% _.each(product.option, function(option) { %>
								<% console.log(option) %>
								<% if((option.option_id == '17')&&($i == 0)) { %>
								<div><small>Тип: Комплект</small></div>
								<% $i++; %>
								<% } else if((option.option_id == '17')&&($i > 0)) { %>
								<div style="display: none"><small>Тип: Комплект</small></div>
								<% } else { %>
								<div><small><%= option.name%>: <%= option.value %></small></div>
								<% } %>
								<% }) %>
							</ul>
							<% if(parseInt(model.config.columns.model)){ %>
								<div class="qc-name-model visible-xs-block"><small><span class="title"><?php echo $column_model; ?>:</span> <span class="text"><%= product.model %></span></small></div>
							<% } %>
							<% if (product.reward) { %>
								<div><small><%= product.reward %></small></div>
							<% } %>
							<% if (product.recurring) { %>
								<div><span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><%= product.recurring %></small></div>
							<% } %>
						</div>


						<div class="block-table__cell block-table__cell--body block-table__cell--count <%= parseInt(model.config.columns.quantity) ? '' : 'hidden' %>">
							<div class="cart-count">
								<div class="jq-number">
									<span class="input-group-btn" style="padding-right: 5%">
										<button class="decrease jq-number__spin minus" data-product="<%= product.key %>"></button>
									</span>
									<input class="jq-number__field" type="text" data-mask="9?999999999999999" value="<%= product.quantity %>" name="cart.<%= product.key %>" min="1" data-refresh="2"/>
									<span class="input-group-btn ">
										<button class="increase jq-number__spin plus" min="1" data-product="<%= product.key %>"></button>

										<button class="btn-danger delete hidden-xs" style="display: none" data-product="<%= product.key %>"><i class="fa fa-times"></i></button>
									</span>
								</div>
							</div>
						</div>

						<div class="block-table__cell block-table__cell--body block-table__cell--price <%= parseInt(model.config.columns.price) && model.show_price  ? '' : 'hidden' %>"><%= product.price %> руб.</div>
						<div class="block-table__cell block-table__cell--body block-table__cell--total <%= parseInt(model.config.columns.total) && model.show_price  ? '' : 'hidden' %>">
							<div class="cart-total"><%= product.total %> руб.</div>
								<div class="jq-number">
									<span class="input-group-btn" style="display: none">
										<button class="decrease jq-number__spin minus" data-product="<%= product.key %>"></button>
									</span>
									<input class="jq-number__field" style="display: none" type="text" data-mask="9?999999999999999" value="<%= product.quantity %>" name="cart.<%= product.key %>" min="1" data-refresh="2"/>
									<span class="input-group-btn ">
										<button class="increase jq-number__spin plus" style="display: none" min="1" data-product="<%= product.key %>"></button>

										<button class="delete remove-link" data-product="<%= product.key %>" style="background-color: white;border: none;outline: none;"></button>
									</span>
								</div>
							</a>
						</div>
			</div>
					<% }) %>

			<div class="form-horizontal">
				<div class=" form-group qc-coupon <%= parseInt(model.config.option.coupon.display) ? '' : 'hidden' %>">
					<% if(model.errors.coupon){ %>
						<div class="col-sm-12">
							<div class="alert alert-danger">
								<i class="fa fa-exclamation-circle"></i> <%= model.errors.coupon %>
							</div>
						</div>
					<% } %>
					<% if(model.successes.coupon){ %>
						<div class="col-sm-12">
							<div class="alert alert-success">
								<i class="fa fa-exclamation-circle"></i> <%= model.successes.coupon %>
							</div>
						</div>
					<% } %>
					<label class="col-sm-4 control-label" >
						<?php echo $text_use_coupon; ?>
					</label>
					<div class="col-sm-8">
						<div class="input-group">
							<input type="text" value="<%= model.coupon ? model.coupon : '' %>" name="coupon" id="coupon" placeholder="<?php echo $text_use_coupon; ?>" class="form-control"/>
							<span class="input-group-btn">
								<button class="btn btn-primary" id="confirm_coupon" type="button"><i class="fa fa-check"></i></button>
							</span>
						</div>
					</div>
					<% _.each(model.coupon, function(voucher) { %>

			        <% }) %>
				</div>
				<div class=" form-group qc-voucher <%= parseInt(model.config.option.voucher.display) ? '' : 'hidden' %>">
					<% if(model.errors.voucher){ %>
						<div class="col-sm-12">
							<div class="alert alert-danger">
								<i class="fa fa-exclamation-circle"></i> <%= model.errors.voucher %>
							</div>
						</div>
					<% } %>
					<% if(model.successes.voucher){ %>
						<div class="col-sm-12">
							<div class="alert alert-success">
								<i class="fa fa-exclamation-circle"></i> <%= model.successes.voucher %>
							</div>
						</div>
					<% } %>

					<label class="col-sm-4 control-label" >
						<?php echo $text_use_voucher; ?>
					</label>
					<div class="col-sm-8">
						<div class="input-group">
							<input type="text" value="<%= model.voucher ? model.voucher : '' %>" name="voucher" id="voucher" placeholder="<?php echo $text_use_voucher; ?>" class="form-control"/>
							<span class="input-group-btn">
								<button class="btn btn-primary" id="confirm_voucher" type="button"><i class="fa fa-check"></i></button>
							</span>
						</div>
					</div>
				</div>
				<?php if($reward_points) {?>
				<div class=" form-group qc-reward <%= parseInt(model.config.option.reward.display) ? '' : 'hidden' %>">
					<% if(model.errors.reward){ %>
						<div class="col-sm-12">
							<div class="alert alert-danger">
								<i class="fa fa-exclamation-circle"></i> <%= model.errors.reward %>
							</div>
						</div>
					<% } %>
					<% if(model.successes.reward){ %>
						<div class="col-sm-12">
							<div class="alert alert-success">
								<i class="fa fa-exclamation-circle"></i> <%= model.successes.reward %>
							</div>
						</div>
					<% } %>
					<label class="col-sm-4 control-label" >
						<?php echo $text_use_reward; ?>
					</label>
					<div class="col-sm-8">
						<div class="input-group">
							<input type="text" value="<%= model.reward ? model.reward : '' %>" name="reward" id="reward" placeholder="<?php echo $text_use_reward; ?>" class="form-control"/>
							<span class="input-group-btn">
								<button class="btn btn-primary" id="confirm_reward" type="button"><i class="fa fa-check"></i></button>
							</span>

						</div>
						<small><?php echo $entry_reward; ?></small>
					</div>

				</div>
				<?php } ?>
			</div>
			</div>
			<div class="total-block">
				<% if(model.show_price){ %>
				<div class="total-block__sum">
					<span>Итого:</span><%= model.totals[0].text %> руб.
				</div>
				<% } %>
			</div>
			<div class="preloader row"><img class="icon" src="image/<%= config.general.loader %>" /></div>
		</div>

</script>
<script>
	$(window).on('load change', function() {
		$('input[type="text"]').each(function(){
			var value = $(this).closest('.cart-count').find('input').val();
			if (value <= 1) {
				$(this).closest('.cart-count').addClass('noactive');
			} else {
				$(this).closest('.cart-count').removeClass('noactive');
			}
		});
	});
$(function() {
	qc.cart = $.extend(true, {}, new qc.Cart(<?php echo $json; ?>));
	qc.cartView = $.extend(true, {}, new qc.CartView({
		el:$("#cart_view"),
		model: qc.cart,
		template: _.template($("#cart_template").html())
	}));

});

</script>