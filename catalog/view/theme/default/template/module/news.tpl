<div class="news">
    <div class="news__title"><?php echo $heading_title; ?></div>
    <div class="news-block">
        <?php foreach ($all_news as $key => $news) { ?>
        <?php if($key <= 1) { ?>
        <div class="news-item">
            <div class="news-item__date"><?php echo $news['date_added']; ?></div>
            <a class="news-item__title" href="<?php echo $news['view']; ?>"><?php echo $news['title']; ?></a>
            <div class="news-item__text">
                <?php echo $news['description']; ?>
            </div>
            <a href="<?php echo $news['view']; ?>" class="news-item__link">Подробнее</a>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
    <a href="index.php?route=information/news" class="news-link">Показать все новости</a>
</div>
