<div class="main__title">Наши популярные товары</div>
<div class="row">
    <div class="tovar-block">
        <?php foreach ($products as $product) { ?>
        <div class="tovar-item">
            <div class="tovar-item__pict"><a href="<?php echo $product['href']; ?>"><img
                            src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"/></a></div>
            <div class="tovar-item__title"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            </div>
            <div class="tovar-item__price">
                <?php if ($product['price']) { ?>
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?> <span>е</span>
                <?php } else { ?>
                <?php echo $product['price']; ?> <span>е</span>
                <?php } ?>
                </p>
                <?php } ?>
            </div>
            <a href="<?php echo $product['href']; ?>" class="tovar-item__link">Подробнее</a>
        </div>
    <?php } ?>
    </div>
</div>
