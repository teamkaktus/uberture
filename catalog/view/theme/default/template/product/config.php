<?php
// HTTP
define('HTTP_SERVER', 'http://uberture/admin/');
define('HTTP_CATALOG', 'http://uberture/');

// HTTPS
define('HTTPS_SERVER', 'http://uberture/admin/');
define('HTTPS_CATALOG', 'http://uberture/');

// DIR
define('DIR_APPLICATION', 'C:/OpenServer/domains/uberture/admin/');
define('DIR_SYSTEM', 'C:/OpenServer/domains/uberture/system/');
define('DIR_LANGUAGE', 'C:/OpenServer/domains/uberture/admin/language/');
define('DIR_TEMPLATE', 'C:/OpenServer/domains/uberture/admin/view/template/');
define('DIR_CONFIG', 'C:/OpenServer/domains/uberture/system/config/');
define('DIR_IMAGE', 'C:/OpenServer/domains/uberture/image/');
define('DIR_CACHE', 'C:/OpenServer/domains/uberture/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/OpenServer/domains/uberture/system/storage/download/');
define('DIR_LOGS', 'C:/OpenServer/domains/uberture/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/OpenServer/domains/uberture/system/storage/modification/');
define('DIR_UPLOAD', 'C:/OpenServer/domains/uberture/system/storage/upload/');
define('DIR_CATALOG', 'C:/OpenServer/domains/uberture/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'uberture');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
