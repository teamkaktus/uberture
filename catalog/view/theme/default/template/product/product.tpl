<?php echo $header; ?>
<div class="content">
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php if($breadcrumb == end($breadcrumbs)) { ?>
    <li><?php echo $breadcrumb['text']; ?></li>
    <?php } else { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <div class="main">
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="main__title"><?php echo $heading_title; ?></div>
      <div class="tovar">
          <?php if ($thumb || $images) { ?>
            <div class="tovar-pict">
                <?php if ($thumb) { ?>
                <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                <?php } ?>
            </div>
          <?php } ?>
        <div class="tovar-features">
          <div class="tovar-available">
              <?php echo $stock; ?>
          </div>
          <div class="tovar-price">
            <div class="tovar-price__item" id="checkAll2">
              <div class="tovar-price__title">Цена за полотно</div>
              <div class="tovar-price__in active">
                  <?php if ($price) { ?>
                    <?php if (!$special) { ?>
                    <?php echo $price; ?><span>е</span>
                    <?php } ?>
                  <?php } ?>
              </div>
            </div>
            <div class="tovar-price__item">
              <?php if ($products) { ?>
              <?php if ($options) { ?>
              <?php foreach ($options as $option) { ?>
              <?php if ($option['type'] == 'checkbox') { ?>
              <div class="tovar-price__title">Цена за комплект</div>
              <?php $total_price = 0; ?>
                          <?php $summ = 0;
                            foreach ($option['product_option_value'] as $option_value) {
                            $summ = $option_value['sum'];
                            } ?>
              <?php $total_price =  $summ + str_replace(' ','',$price)?>
              <div class="tovar-price__in" id="checkAll"><?= (number_format($total_price, 0, '.', ' ')); ?><span>е</span>
                <div class="tooltip-complect">
                  <i class="tooltip-icon"></i>
                  <div class="tooltip">
                    <ul class="tooltip-list">
                      <li>
                        <div id="product">
                          <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label"><?php echo $option['name']; ?></label>
                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                              <span class="tooltip-list__count"> 1 шт.</span>
                              <span class="tooltip-list__name"><?php echo $heading_title; ?></span>
                              <span class="tooltip-list__price">
                            <?php if ($price) { ?>
                                <?php if (!$special) { ?>
                                <?php echo $price; ?><span>руб.</span>
                                <?php } ?>
                                <?php } ?>
                            </span>
                              <?php foreach ($option['product_option_value'] as $option_value) { ?>
                              <div class="checkbox">
                                <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                <label >
                                  <span class="tooltip-list__count"><?php echo $option_value['quantity']; ?> шт.</span>
                                  <span class="tooltip-list__name"><?php echo $option_value['name']; ?></span>
                                  <?php if ($option_value['price']) { ?>
                                  <span class="tooltip-list__price"><?php echo $option_value['price']; ?> руб.</span>
                                  <?php } ?>
                                </label>
                              </div>
                              <?php } ?>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <?php } ?>
              <?php } ?>
              <?php } ?>
              <?php } ?>
            </div>
              <a id="product">
                <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" size="2"
                       id="input-quantity" class="form-control"/>
                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                <button type="button" class="btn btn--cart" style="margin-left: 14px;cursor:pointer;border: none;" id="button-cart" data-effect="mfp-zoom-in">
                  <p>В корзину</p>
                </button>
              </a>
            <div id="product">
              <?php if ($options) { ?>
              <?php foreach ($options as $option) { ?>
              <?php if ($option['option_id'] == '15') { ?>
              <div class="">
              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>" style="margin-top: 17px;">
                <label class="change-block__title">ВЫБЕРИТЕ НУЖНЫЙ РАЗМЕР</label>
                <div class="class_clas" id="input-option<?php echo $option['product_option_id']; ?>">
                  <?php foreach ($option['product_option_value'] as $option_value) { ?>
                  <div class="radio change-size" style="width: 76px;height: 48px;float: left;margin-right: 3px;margin-bottom: 19px;">
                    <label class="my_clas2" id="<?php echo $option_value['name']; ?>">
                      <input type="radio"
                             name="option[<?php echo $option['product_option_id']; ?>]"
                             value="<?php echo $option_value['product_option_value_id']; ?>"/>
                      <a class="change-size__item2"
                         alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>">
                        <?php echo $option_value['name'] ?></a>
                    </label>
                  </div>
                  <?php } ?>
                </div>
              </div>
              </div>
              <?php } ?>
              <?php } ?>
            </div>
            <div id="product">
              <?php foreach ($options as $option) { ?>
              <?php if ($option['option_id'] == '13') { ?>
              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="change-block__title">ВЫБЕРИТЕ НУЖНЫЙ ЦВЕТ</label>
                <div class="class_clas" id="input-option<?php echo $option['product_option_id']; ?>">
                  <?php foreach ($option['product_option_value'] as $option_value) { ?>
                    <div class="radio change-block--tovar tova_nth">
                      <label class="my_clas" id="<?php echo $option_value['name']; ?>">
                        <input type="radio"
                               name="option[<?php echo $option['product_option_id']; ?>]"
                               value="<?php echo $option_value['product_option_value_id']; ?>"/>
                        <a class="change-block__item">
                        <img src="<?php echo $option_value['image']; ?>"
                             alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                             /></a>
                      </label>
                    </div>
                  <?php } ?>
                </div>
              </div>
              <?php } ?>
              <?php } ?>
            </div>
            <div id="product">
              <?php foreach ($options as $option) { ?>
              <?php if ($option['option_id'] == '16') { ?>
              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="change-block__title" style="position: relative;top: 15px;">ВЫБЕРИТЕ СТЕКЛО</label>
                <div class="class_clas2" id="input-option<?php echo $option['product_option_id']; ?>">
                  <?php foreach ($option['product_option_value'] as $option_value) { ?>
                  <div class="radio change-block--tovar tova_nth">
                    <label class="my_clas" id="<?php echo $option_value['name']; ?>">
                      <input type="radio"
                             name="option[<?php echo $option['product_option_id']; ?>]"
                             value="<?php echo $option_value['product_option_value_id']; ?>"/>
                      <a class="change-block__item">
                        <img src="<?php echo $option_value['image']; ?>"
                             alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                        /></a>
                    </label>
                  </div>
                  <?php } ?>
                </div>
              </div>
              <?php } ?>
              <?php } ?>
              <?php } ?>
            </div>
            <div style="clear: both"></div>
            <div class="tab-block">
              <a href="#" class="tab-block__item" data-target="#tab-content-1">Характеристики</a>
              <a href="#" class="tab-block__item" data-target="#tab-content-2">Комплектующие</a>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-body">
        <div id="tab-content-1" class="tab-content">
          <ul class="descr-list">
            <?php if ($attribute_groups) { ?>
            <?php foreach ($attribute_groups as $attribute_group) { ?>
            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
            <li>
              <span class="descr-list__name"><?php echo $attribute['name']; ?></span>
              <span class="descr-list__main"><?php echo $attribute['text']; ?></span>
            </li>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </ul>
        </div>
        <div id="tab-content-2" class="tab-content">
        <?php if ($products) { ?>
          <?php foreach ($products as $product) { ?>
          <div class="complect-item">
              <div class="complect-item__pict"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"/></div>
                <a class="complect-item__title" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                <div class="complect-item__price">
                  <?php if ($price) { ?>
                  <?php if (!$special) { ?>
                  от <?php echo $price; ?> руб.
                  <?php } ?>
                  <?php } ?>
                </div>
          </div>
          <?php } ?>
        <?php } ?>
        </div>
      </div>
      <div class="tovar-descr">
        <p><?php echo $description; ?></p>
      </div>
      <?php foreach ($banners as $banner) { ?>
      <a class="banner" rel="fancybox-button" href="<?php echo $banner['full_img']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>"/></a>
      <?php } ?>
    </div>

    </div>
    <?php echo $column_right; ?></div>
</div>
</div>
<div style="clear: both"></div>
<script type="text/javascript"><!--
  $('#checkAll').click(function(){
    $('input:checkbox').prop('checked','checked');

    if ($('input:checkbox').is(':checked')) {
      $('input:checkbox').prop('checked',true);
    }
  });
  //--></script>
<script type="text/javascript"><!--
  $('#checkAll2').click(function(){
    $('input:checkbox').prop('checked','checked');

    if ($('input:checkbox').is(':checked')) {
      $('input:checkbox').prop('checked',false);
    }
  });
  //--></script>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
  $('#button-cart').on('click', function() {
    $.ajax({
      url: 'index.php?route=checkout/cart/add',
      type: 'post',
      data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
      dataType: 'json',
      beforeSend: function() {
        $('#button-cart').button('loading');
      },
      complete: function() {
        $('#button-cart').button('reset');
      },
      success: function(json) {
        $('.alert, .text-danger').remove();
        $('.form-group').removeClass('has-error');

        if (json['error']) {
          if (json['error']['option']) {
            for (i in json['error']['option']) {
              var element = $('#input-option' + i.replace('_', '-'));

              if (element.parent().hasClass('input-group')) {
                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
              } else {
                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
              }
            }
          }

          if (json['error']['recurring']) {
            $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
          }

          // Highlight any found errors
          $('.text-danger').parent().addClass('has-error');
        }

        if (json['success']) {
          $('.breadcrumb').after('<div class="alert alert-success"><div class="mfp-bg mfp-zoom-in mfp-ready"></div><div class="mfp-wrap mfp-close-btn-in mfp-auto-cursor mfp-zoom-in mfp-ready" tabindex="-1" style="overflow-x: hidden; overflow-y: auto;"><div class="mfp-container mfp-s-ready mfp-inline-holder"><div class="mfp-content"><div id="add-cart" class="add-cart mfp-with-anim"><div class="add-cart__title">' + json['success'] + '</div><button type="button" class="close22" data-dismiss="alert">&times;</button></div></div></div></div></div>');

          setTimeout(function () {
            $('.cart__number').html('<span> ' + json['total'] + '</span> товаров');
          }, 100);// Need to set timeout otherwise it wont update the total
          setTimeout(function () {
            $('.cart__tprice').html('<span> ' + json['total2'] + '</span> руб');
          }, 100);

          $('html, body').animate({ scrollTop: 0 }, 'slow');

          $('#cart > ul').load('index.php?route=common/cart/info ul li');
        }
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>
