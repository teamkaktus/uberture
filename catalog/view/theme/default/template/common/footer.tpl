<footer class="footer">
  <div class="container">
    <div class="footer-block">
      <a href="#" class="footer-block__logo"><img src="img/logo-footer.png" alt=""></a>
      <div class="footer-block__location">
        <?php echo $findTouch; ?>
      </div>
      <a onclick="return showModal(this);" style="cursor: pointer" class="footer-block__link">Схема проезда</a>
      <div class="footer-block__phone">
        <a href="mailto:<?php echo $telephone; ?>" class="footer-block__tel">
          <?php echo $telephone; ?>

        </a>,
        <a href="mailto:<?php echo $telephone2; ?>" class="footer-block__tel">
          <?php echo $telephone2; ?>
        </a>
      </div>
      <a href="mailto:<?php echo $findEmail; ?>" class="footer-block__mail">
        <?php echo $findEmail; ?>
      </a>
      <div class="social-block">
        <span><?= $Subscribe_to_us ?>:</span>
        <a href="<?php echo $instagram; ?>" class="social-block__link social-block__link--inst">instagram</a>
        <a href="<?php echo $facebook; ?>" class="social-block__link social-block__link--fb">facebook</a>
        <a href="<?php echo $vk; ?>" class="social-block__link social-block__link--vk">vkontakte</a>
      </div>
      <a id="email_contact" href="#" class="link__letter" data-toggle="modal" data-target="#contactForm2"><?= $Write_a_letter ?></a>
    </div>

    <div class="footer-menu footer-menu--about">
      <?php if ($informations) { ?>
      <div class="footer-menu__title">
        <?php echo $text_information; ?>
      </div>
      <ul class="footer-nav">
        <?php foreach ($informations as $information) { ?>
        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <?php } ?>
    <div class="footer-menu footer-menu--catalog">
      <div class="footer-menu__title footer-menu__title--low"><?= $catalog?></div>
      <ul class="footer-nav">
        <?php foreach ($categories as $category) { ?>
        <li><a href="<?php echo $category['href']; ?>"> <?php echo $category['name']; ?></a></li>
        <?php } ?>

      </ul>
    </div>

    <div class="footer-menu footer-menu--order">
      <div class="footer-menu__title footer-menu__title--up"><?= $How_to_order; ?></div>
      <ul class="footer-nav">
        <li><a href="#">Порядок заказа</a></li>
        <li><a href="#">Наши гарантии</a></li>
        <li><a href="#">Доставка и монтаж</a></li>
        <li><a href="#">Вопрос-ответ</a></li>
        <li><a href="#">Вопрос-ответ</a></li>
      </ul>
    </div>

    <div class="footer-menu">
      <div class="footer-menu__title footer-menu__title--up"><?= $Our_works; ?></div>
      <ul class="footer-nav">
        <li><a href="#">Установленные двери</a></li>
        <li><a href="#">Статьи</a></li>
      </ul>
    </div>

  </div>
</footer>
<div id="contactForm2" class="modal fade in popup2">
  <div class="modal-dialog">
    <div class="modal-content modal-div-style">
      <div class="modal-header" style="border: 0;float: right;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" style="text-align: center;">
        <div class="modal-title-style">Перезвоните мне!</div>
        <div class="form-width-style">
            <ul id="errorMasege" style="padding-left: 0">
              <!--<span style="display: none; color: red" class="row name">Некоректно заполнено Имя пользователя</span>-->
              <div>
                <span id="err_e_name" style="display: none; color: red;">Некоректно заполненно имя</span>
              </div>
              <div>
                <span id="err_e_email" style="display: none; color: red">Некоректно введен E-mail</span>
              </div>
              <div>
                <span id="err_e_massage" style="display: none; color: red">Некоректно введенно сообщение</span>
              </div>
            </ul>
            <form id="modal_email_1">
            <div class="div-width-100">
              <div class="div-width-100 input-title-style">
                <span>Ваше имя *:</span>
              </div>
              <input class="input-style contactItem text" id="email_name" name="email_name" type="text">
            </div>
            <div class="div-width-100">
              <div class="div-width-100 input-title-style">
                <span>Ваш email *:</span>
              </div>
              <input class="input-style contactItem text" id="email_email" name="email_email" type="text">
            </div>
            <div class="div-width-100 ">
              <div class="div-width-100 input-title-style-2">
                <span>Ваше сообщение *:</span>
              </div>
              <textarea class="input-style input_3style33 contactItem text" id="email_massage" name="email_massage" type="text"></textarea>
            </div>
              </form>
            <div class="div-width-100 modal-btn-div-style">
              <div class="modal-btn-img-style">
                <button id="email_send" type="button" class="btn btn_otp">Отправить</button>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/common2.js" type="text/javascript"></script>

<script src="catalog/view/javascript/libs/modernizr/modernizr.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/formstyler/jquery.formstyler.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/touchSwipe/jquery.touchSwipe.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/libs/slick/slick.min.js" type="text/javascript"></script>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>