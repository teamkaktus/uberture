<?php echo $header; ?>
<div class="slider-wrap">
    <div class="slider-nav container"></div>
    <div class="home-slider">
        <?php foreach ($banners as $banner) { ?>
        <div class="slider-item">
            <a href="<?php echo $banner['link']; ?>">
                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>"/>
            </a>
        </div>
        <?php } ?>
    </div>
</div>
<?php echo $content_top; ?>
<div class="content">
    <div class="container">
        <?php echo $column_left; ?>
        <div class="main">
            <?php echo $column_right; ?>
        </div>
    </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>