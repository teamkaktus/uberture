<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/sweetalert2/6.5.5/sweetalert2.min.js" type="text/javascript"></script>
  <!--<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />-->
  <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
  <!--<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">-->
<link href="catalog/view/theme/default/stylesheet/sprite.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/fonts.css" rel="stylesheet">
  <link href="catalog/view/theme/default/stylesheet/contactForm.css" rel="stylesheet">
  <link href="catalog/view/javascript/libs/normalize/normilize.css"  rel="stylesheet"  type="text/css">
  <link href="catalog/view/javascript/libs/magnific/magnific-popup.css"  rel="stylesheet"  type="text/css">
  <link href="catalog/view/javascript/libs/formstyler/jquery.formstyler.css"  rel="stylesheet"  type="text/css">
  <link href="catalog/view/javascript/libs/slick/slick.css"  rel="stylesheet"  type="text/css">
  <link href="https://cdn.jsdelivr.net/sweetalert2/6.5.5/sweetalert2.min.css" rel="stylesheet">

  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
  <script src="catalog/view/javascript/contactForm.js" type="text/javascript"></script>

<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<header class="header">
  <div class="header-top">
    <div class="container">
      <a href="index.php?route=information/information&information_id=9" class="sale-link">Акции</a>
      <ul class="menu-top">
        <li class="menu-top__item"><a href="index.php?route=information/information&information_id=4">О нас</a></li>
        <li class="menu-top__item"><a href="index.php?route=information/news">Новости</a></li>
        <li class="menu-top__item"><a href="index.php?route=information/gallery">Каталог</a></li>
        <li class="menu-top__item"><a href="index.php?route=information/information&information_id=7">Как заказать</a></li>
        <li class="menu-top__item"><a href="index.php?route=information/information&information_id=8">Вопрос-ответ</a></li>
        <li class="menu-top__item"><a href="index.php?route=information/contact">Контакты</a></li>
      </ul>
    </div>
  </div>
  <div class="header-main">

    <div class="container">
      <span class="logo">
        <?php if ($logo) { ?>
        <?php if ($home == $og_url) { ?>
        <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/></a>
        <?php } else { ?>
        <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"/></a>
        <?php } ?>
        <?php } else { ?>
        <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
        <?php } ?>
      </span>
      <div class="cart"><?php echo $cart; ?></div>
      <div class="work">
        <p><?php echo $open; ?></p>
      </div>
      <div class="location location--header">
        <p><span><?php echo $address; ?></span><?php echo $address2; ?></p>
        <a  onclick="return showModal(this);" class="location__link" style="cursor: pointer">Схема проезда</a>
        <div id="modalWindow" class="modal-window">
          <div class="modal-window-close" title="Закрить" onclick="this.parentNode.style.display='none';">X</div>
          <div id="map" style="width: 100%; height: 400px; margin-bottom: 20px" ></div>
          <div class="modal-window-content">
            <iframe src="about:blank" frameborder="0"></iframe>
          </div>
        </div>
      </div>
      <div class="contact contact--header">
        <div class="contact-wrap">
          <a href="mailto:+73432478248" class="contact__phone contact__phone--header"><?php echo $telephone; ?></a>
          <a href="mailto:+73432478248" class="contact__phone contact__phone--header"><?php echo $telephone2; ?></a>
          <a href="#" class="contact__callback" data-toggle="modal" data-target="#contactForm">Перезвоните мне!</a>
          <a href="mailto:<?php echo $email; ?>" class="contact__mail"><?php echo $email; ?></a>
        </div>
      </div>
    </div>

  </div>
  <?php if(($currentRout == 'common/home')||($currentRout == '')) { ?>
  <div class="header-bottom">
    <div class="container">
      <ul class="menu-main">
        <?php foreach ($categories as $key => $category) { ?>
        <?php if($key <= 3) { ?>
        <li class="menu-main__item"><a href="<?= $category['href']; ?>"><?= $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
        <li class="menu-main__item">
          <a href="#">Наши услуги<i class="caret"></i></a>
          <ul class="menu-dropmain">
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?= $category['href']; ?>"><?= $category['name']; ?></a></li>
            <?php }?>
          </ul>
        </li>
        <li class="menu-main__item"><a href="index.php?route=information/gallery">Галерея</a></li>
      </ul>
    </div>
  </div>
  <?php }else{ ?>
  <div class="header-bottom header-bottom--inner">
    <div class="container">
      <ul class="menu-main">
        <?php foreach ($categories as $category) { ?>
        <li class="menu-main__item"><a href="<?= $category['href']; ?>"><?= $category['name']; ?></a></li>
        <?php }?>
        <li class="menu-main__item">
          <a href="#">Наши услуги<i class="caret"></i></a>
          <ul class="menu-dropmain">
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?= $category['href']; ?>"><?= $category['name']; ?></a></li>
            <?php }?>
          </ul>
        </li>
        <li class="menu-main__item"><a href="index.php?route=information/gallery">Галерея</a></li>
      </ul>
    </div>
  </div>
  <?php } ?>
</header>
<div id="contactForm" class="modal fade in popup2">
  <div class="modal-dialog">
    <div class="modal-content modal-div-style">
      <div class="modal-header" style="border: 0;float: right;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" style="text-align: center;">
        <div class="modal-title-style">Перезвоните мне!</div>
        <div class="form-width-style">
          <form id="contactForm_c" action="" method="post">
            <ul id="errorMasege" style="padding-left: 0">
              <!--<span style="display: none; color: red" class="row name">Некоректно заполнено Имя пользователя</span>-->
              <div style="display: none; color: red;text-align: center;" class="row name">Введите Имя</div>
              <div style="display: none; color: red;text-align: center;" class="row phone">Некоректно заполнен номер телефона</div>
            </ul>
            <div class="div-width-100">
              <div class="div-width-100 input-title-style">
                <span>Ваше имя *:</span>
              </div>
              <input class="input-style contactItem text" id="name" name="name" type="text">
            </div>
            <div class="div-width-100 ">
              <div class="div-width-100 input-title-style-2">
                <span>Ваш телефон *:</span>
              </div>
              <input class="input-style contactItem text" id="phone" name="phone" type="text">
            </div>
            <div class="div-width-100 modal-btn-div-style">
              <div class="modal-btn-img-style">
                <button id="send" type="button" class="btn btn_otp">Отправить</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  ymaps.ready(init);
  var myMap,
          myPlacemark;

  function init(){
    myMap = new ymaps.Map("map", {
      center: [56.862174, 60.620714],
      zoom: 16
    });

    myPlacemark = new ymaps.Placemark([56.862174, 60.620714], {
      hintContent: 'Екатеринбург ул. Красина, 3',
      balloonContent: 'Столица России'
    });

    myMap.geoObjects.add(myPlacemark);
  }
</script>
<!--/*<?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?>*/-->
