<!-- Samdev http://free-it.ru -->
<?php echo $header; ?>
<div class="content2">
<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php if($breadcrumb == end($breadcrumbs)) { ?>
		<li><?php echo $breadcrumb['text']; ?></li>
		<?php } else { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
		<?php } ?>
	</ul>
  	<div class="row">
		<div class="col-xs-12">
			<div class="content-in">
				<div class="row">
				  	<?php echo $column_left; ?>
				    <?php if ($column_left && $column_right) { ?>
				    <?php $class = 'col-sm-6'; ?>
				    <?php } elseif ($column_left || $column_right) { ?>
				    <?php $class = 'col-sm-9'; ?>
				    <?php } else { ?>
				    <?php $class = 'col-sm-12'; ?>
				    <?php } ?>
					<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
						<div class="news__title"><?php echo $heading_title; ?></div>
						<div class="news-article2 clearfix">
							<?php if ($image) { ?>
							<img class="single-image pull-left" src="<?php echo $image; ?>" alt="<?php echo $heading_title; ?>" />
							<?php } ?>
							<?php echo $description; ?>
						</div>
						<?php echo $content_bottom; ?>
					</div>
				    <?php echo $column_right; ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php echo $footer; ?>