<?php echo $header; ?>
<div class="content2">
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if($breadcrumb == end($breadcrumbs)) { ?>
        <li><?php echo $breadcrumb['text']; ?></li>
        <?php } else { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
        <?php } ?>
    </ul>
    <div class="content" style="padding-top: 0">
            <?php echo $column_left; ?>
            <div class="main">
                <div class="main__title">Наша галерея</div>
                <div class="gallery">
                    <?php foreach ($banners as $banner) { ?>
                        <a title="<?php echo $banner['title']; ?>" class="fancybox-button gallery-pict" rel="fancybox-button" href="<?php echo $banner['full_img']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>"/></a>
                        <span class="product_span spanprod"><?php echo  $banner['title']; ?></span>
                    <?php } ?>
                </div>
            </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox-button").fancybox({
            prevEffect		: 'none',
            nextEffect		: 'none',
            closeBtn		: false,
            helpers		: {
                title	: { type : 'inside' },
                buttons	: {}
            }
        });
    });
</script>
<?php echo $footer; ?>