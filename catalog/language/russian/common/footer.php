<?php
// Text
$_['Subscribe_to_us']   = 'Подпишитесь на нас';
$_['Write_a_letter']    = 'Написать письмо';
$_['Our_works']         = 'НАШИ РАБОТЫ';
$_['How_to_order']      = 'КАК ЗАКАЗАТЬ';
$_['catalog']           = 'КАТАЛОГ';
$_['text_information']  = 'О нас';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Связаться с нами';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнёры';
$_['text_special']      = 'Товары со скидкой';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Мои Закладки';
$_['text_newsletter']   = 'Рассылка новостей';
$_['text_powered']      = 'Работает на <a target="_blank" href="http://myopencart.com/">ocStore</a><br /> %s &copy; %s';